# AJAX Example

This example illustrates a working AJAX request when both client and server are running on local host. 

Developing Web Apps and Services Fall 2017

## Install server dependencies

Server-side dependencies are listed in package.json. 

Use "Open command window here as administrator" to open a command prompt in the **server** folder. 

Run:

```
npm install
```

## Start the server

Use "Open command window here as administrator" to open a command prompt in the **server** folder. 

Run either node or nodemon:

```
node app.js
```

```
nodemon app.js
```

## Verify the server

Open a browser to test both GET URIs:

- http://localhost:8085
- http://localhost:8085/fortune

## Start the client

Open the client/index.html file in a browser, e.g., right-click and select Open with Chrome. 



